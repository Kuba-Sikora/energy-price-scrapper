const requestPromise = require('request-promise')
const cheerio = require('cheerio')
const ModbusRTU = require('modbus-serial')
require('dotenv').config()

const url = 'https://www.ote-cr.cz/cs/kratkodobe-trhy/elektrina/denni-trh?'
const plcAddress = process.env.PLC_IP_ADDRESS
const plcPort = process.env.PLC_PORT
const startingModbusAddress = process.env.STARTING_MODBUS_ADDRESS

if (!(plcAddress && plcPort && startingModbusAddress)) throw new Error('Parametry jsou špatně nastavené')

const getPrices = async () => {
  try {
    res = await requestPromise(url)
    const $ = cheerio.load(res)
    const prices = []
    $('.bigtable').find('.report_table').children('tbody').children('tr').each((_i, el) => {
      // test the th elements with an array from 1-24
      if ([...Array(25).keys()].filter(i => i !== 0).includes(parseInt($(el).find('th').text()))) {
        value = $(el).children('td').first().text() // get the first column in the row which is the price
        prices.push(value)
      }
    })
    return prices.map(price => parseFloat(price.replace(',', '.'))) // convert to float array
  } catch (e) {
    console.error(e)
  }
}

const createModbusClient = async () => {
  const client = new ModbusRTU()
  await client.connectTCP(plcAddress, { port: plcPort })
  client.setID(1)
  if (!client.isOpen) {
    throw new Error('modbus client not open')
  }
  return client
}

const exportPrices = async (client, prices) => {
  await client.writeRegisters(startingModbusAddress, prices.map(price => price * 100)) // get rid of the floating point
  return await client.readHoldingRegisters(startingModbusAddress, prices.length, (err, data) => {
    console.log(data.data)
    if (err) console.error(err)
  })
}

(async () => {
  const client = await createModbusClient()
  const prices = await getPrices()

  await exportPrices(client, prices)
  return
})()
